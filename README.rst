Liar's dice 
===========

This code implements a framework for testing Liar's dice players as suggested
by Neej.

The script liars.py will play Liar's dice. By default it allows you to play
against the AI opponents defined in the players directory.

Two AI players are already implemented in players. Have a look at those to see
how to implement your own.

Interface
---------

Each AI player read from stdin and writes to stdout. The format recieved by
the player on stdin as as follows. Every line is of the form::

    <type>: <details>

The basic idea is that your program reads lines from stdin one at a time until
it finds the go! line. At this point it writes the go to stdout.

First there are the header lines. These look like::

    players: 4
    player: dan
    player: yani
    player: neej
    player: petros
    you: yani

These lines show

 * the number of players (4)
 * the name of each player (e.g. "dan")
 * the order of the players (dan, yani, ...)
 * the first player (dan)
 * which of the players you are (2 e.g. yani)

After these lines it is the start of the game and everyone has 5 dice. The
game then loops through rounds. Each round consists of a round header that
looks like::

    round: 1
    dice: 0 3 0 0 1 1

This shows that it is round 1 and that your dice are three twos a five and a
six.

Within each round we then loop through the turns. A turn looks like the
following::

    turn: dan bet 3 4
    turn: yani bluff
    turn: neej spoton

When it is your turn, you will receive a line that looks like::

    go!:

Following a bluff or a spoton all dice will be announced. This looks like::

    showdice: dan 0 3 0 0 1 1
    showdice: yani 1 0 0 0 0 0
    showdice: neej 3 0 0 2 0 0
    showdice: petros 0 0 1 1 0 3

At this point one or more people will lose dice and some may need to leave the
game. This looks like::

    losedice: dan 4
    losedice: yani 0
    losedice: petros 4
    out: yani

This means that dan and petros now have 4 dice and yani is out of the game.
Finally the game ends with::

    winner: petros

which has the obvious meaning. A complete game is shown below from the
perspective of oscar playing against bluffer and raiser::

    $ ./liars.py --human=oscar
    players: 3
    player: oscar
    player: raiser
    player: bluffer
    you: oscar
    round: 1
    dice: 2 4 5 2 2
    go: now!
    2 2                     <------ these lines are output not input
    turn: oscar 2 2
    turn: raiser 3 2
    turn: bluffer bluff
    showdice: oscar 0 3 0 1 1 0
    showdice: raiser 0 1 1 2 1 0
    showdice: bluffer 3 0 0 1 1 0
    dicelost: bluffer 4
    round: 2
    dice: 6 2 4 4 1
    turn: bluffer 1 1
    go: now!
    4 1
    turn: oscar 4 1
    turn: raiser 5 1
    turn: bluffer bluff
    showdice: oscar 1 1 0 2 0 1
    showdice: raiser 0 2 1 0 0 2
    showdice: bluffer 1 1 1 0 0 1
    dicelost: raiser 4
    round: 3
    dice: 3 5 4 5 4
    turn: raiser 1 1
    turn: bluffer bluff
    showdice: oscar 0 0 1 2 2 0
    showdice: raiser 1 0 2 0 0 1
    showdice: bluffer 0 1 2 0 1 0
    dicelost: bluffer 3
    round: 4
    dice: 1 6 2 6 5
    turn: bluffer 1 1
    go: now!
    3 2
    turn: oscar 3 2
    turn: raiser 4 2
    turn: bluffer bluff
    showdice: oscar 1 1 0 0 1 2
    showdice: raiser 0 0 0 2 1 1
    showdice: bluffer 1 0 1 0 0 1
    dicelost: raiser 3
    round: 5
    dice: 4 4 3 2 5
    turn: raiser 1 1
    turn: bluffer bluff
    showdice: oscar 0 1 1 2 1 0
    showdice: raiser 0 0 0 0 1 2
    showdice: bluffer 0 1 0 1 0 1
    dicelost: raiser 2
    round: 6
    dice: 4 6 2 5 5
    turn: raiser 1 1
    turn: bluffer bluff
    showdice: oscar 0 1 0 1 2 1
    showdice: raiser 1 1 0 0 0 0
    showdice: bluffer 1 0 0 1 0 1
    dicelost: bluffer 2
    round: 7
    dice: 3 3 6 3 1
    turn: bluffer 1 1
    go: now!
    3 2
    turn: oscar 3 2
    turn: raiser 4 2
    turn: bluffer bluff
    showdice: oscar 1 0 3 0 0 1
    showdice: raiser 0 0 0 0 1 1
    showdice: bluffer 0 0 1 0 0 1
    dicelost: raiser 1
    round: 8
    dice: 5 1 5 2 1
    turn: raiser 1 1
    turn: bluffer bluff
    showdice: oscar 2 1 0 0 2 0
    showdice: raiser 1 0 0 0 0 0
    showdice: bluffer 0 0 1 0 1 0
    dicelost: bluffer 1
    round: 9
    dice: 1 1 1 3 4
    turn: bluffer 1 1
    go: now!
    3 2
    turn: oscar 3 2
    turn: raiser 4 2
    turn: bluffer bluff
    showdice: oscar 3 0 1 1 0 0
    showdice: raiser 1 0 0 0 0 0
    showdice: bluffer 0 0 1 0 0 0
    dicelost: raiser 0
    out: raiser
    round: 10
    dice: 5 3 5 5 5
    turn: bluffer 1 1
    go: now!
    4 5
    turn: oscar 4 5
    turn: bluffer bluff
    showdice: oscar 0 0 1 0 4 0
    showdice: bluffer 0 0 0 0 1 0
    dicelost: bluffer 0
    out: bluffer
    winner: oscar 

