#!/usr/bin/env python

import sys


def log(msg):
    sys.stderr.write('raiser -> ' + msg + '\n')
    sys.stderr.flush()

# Read a line from stdin
def gets():
    line = sys.stdin.readline().strip()
    return line

# Write a line to stdout
def puts(line):
    print(line)
    sys.stdout.flush()  # This is important to prevent deadlocks

# Allows to iterate over the input lines
def iter_input():
    while True:
        line = gets()
        try:
            action, data = line.split(': ')
        except ValueError:
            print("Confused. Quitting...")
            sys.exit(-1)
        yield action, data

# Now read the main data
for action, data in iter_input():

    # Start of the game
    if action == 'game':

        # Read the game header
        gamenum = int(data)
        players = []
        for action, data in iter_input():
            if action == 'players':
                numplayers = int(data)
            elif action == 'player':
                players.append(data)
            elif action == 'you':
                myname = data
                break
            else:
                raise ValueError("I'm confused!")

        # Data structures to keep track of what everyone else is doing
        num_dice = dict((name, 5) for name in players)
        who_bluffs = dict((name, 0) for name in players)
        who_spotons = dict((name, 0) for name in players)

    # Announced at the start of each round
    elif action == 'round':
        round_num = int(data)
        last_bet = None

    # This line always follows the round line
    elif action == 'dice':
        mydice = dict((n, int(s)) for n, s in enumerate(data.split(), 1))

    # Someone (possibly me) has had their turn
    elif action == 'turn':
        name, decision = data.split(' ', 1)

        # I only want to raise all the time so I just need to keep track of
        # whether or not there is anything to raise. This shows how to keep
        # track of what the players are doing though in case that was waht you
        # wanted to do.
        if decision == 'bluff':
            who_bluffs[name] += 1
        elif decision == 'spoton':
            who_spotons[name] += 1
        else:
            try:
                num, face = [int(s) for s in decision.split()]
            except ValueError:
                # Invalid move
                continue
            last_bet = num, face

    # My go: raise something
    elif action == 'go':
        if last_bet is None:
            log("Can't raise. Play it safe...")
            puts('1 1')  # Wussy bet cause there's nothing to raise
        else:
            log("It's raising time!!!!!!!!!")
            num, face = last_bet
            puts('{0} {1}'.format(num + 1, face))

    # Now we get to see peoples dice
    elif action == 'showdice':
        name, nums = data.split(' ', 1)
        nums = [int(s) for s in nums.split()]
        # Maybe we could do something interesting with these numbers

    # Someone lost a dice (possibly me)
    elif action == 'dicelost':
        name, newnum = data.split()
        num_dice[name] = int(newnum)
        if name == 'myname':
            log('Bollocks!!!!!!!!!!!!!!!')
            pass

    # Someone is now out of the game!
    elif action == 'out':
        name = data
        del num_dice[name]  # Wipe them from memory

    # End of the game
    elif action == 'winner':
        winner = data

    # End of all games - exit nicely
    elif action == 'gameover':
        break

    # Just ignore actions that we don't recognise. This ensures that the
    # player is future-proof if the server adds more information output.
    else:
        pass
