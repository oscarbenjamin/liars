#!/usr/bin/env python

import sys

# Write random information to the log
def log(msg):
    sys.stderr.write('bluff -> ' + msg + '\n')
    sys.stderr.flush()

# Read a line of input from the game server
def gets():
    line = sys.stdin.readline().strip()
    return line

# Write a response back to the game server
def puts(line):
    print(line)
    sys.stdout.flush() # Need to flush to prevent deadlocks

# bluffer just bluffs all the time. However, you can't bluff if it's the first
# go in a round so it needs to keep track of whether its the first go in a
# round and have a fallback move for that situation.
while True:
    # Empty line happens at the end of all input
    line = gets()
    try:
        action, data = line.split(': ')
    except ValueError:
        print("Confused. Quitting...")
        sys.exit(-1)

    # Need to keep track of when we can bluff and when we can't
    if action == 'round':
        new_round = True
    elif action == 'turn':
        new_round = False
    elif action == 'go':
        if not new_round:
            log("Let's bluff!!!!!!!!!!!!")
            decision = 'bluff'
        else:
            log("Can't bluff... :(")
            decision = '1 1'
        puts(decision)

    # Exit nicely when done
    elif action == "gameover":
        break
