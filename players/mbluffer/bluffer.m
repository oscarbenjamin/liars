%#!/usr/bin/env octave
%
% The line below can be used when debugging to force input from a different
% file. To run on the server it needs to read from stdin

fin = stdin;
fout = stdout;
logfile = stderr;

% bluffer just bluffs all the time. However, you can't bluff if it's the first
% go in a round so it needs to keep track of whether its the first go in a
% round and have a fallback move for that situation.
while 1
    line = fgetl(stdin);

    [action, data] = strtok(line, ': ');
    data = data(3:end);  % ditch the ': ' at the beginning
    
    % Keep track of whether it is the first go in a round
    if strcmp(action, 'round')
        new_round = 1;
    elseif strcmp(action, 'turn')
        new_round = 0;
    % Make our move
    elseif strcmp(action, 'go')
        if ~new_round
            % For debugging:
            fprintf(logfile, ['mbluf -> Lets bluff!!!!!!\n']);
            fflush(logfile);
            decision = 'bluff';
        else
            fprintf(logfile, ['mbluf -> Cant bluff :(\n']);
            fflush(logfile);
            decision = '1 1';
        end
        fprintf(fout, [decision '\n']);
        fflush(fout); % need to flush stdout to prevent deadlocks

    % Gameover. Presumably we won!
    elseif strcmp(action, 'gameover')
        break
    end
end
