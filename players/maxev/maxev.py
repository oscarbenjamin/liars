#!/usr/bin/env python
#
# maxev.py
#
# A Liar's dice player that bases decisions on maximising the expected value
# of the outcome assuming similar or simplistic behaviour from its opponents.
# In particular this player makes no attempt to draw inferences from its
# oppenent's bids.

from __future__ import print_function

import sys
import os
import functools
from fractions import Fraction


# Are we in debug mode?
DEBUG = 'DEBUG' in os.environ

# ::::::
#   Input output functions
# ::::::

INPUT = sys.stdin

def gets():
    '''Retrieve a line from the game server'''
    line = INPUT.readline().strip()
    debug('maxev -> received: "%s"' % line)
    return line

def puts(line):
    '''Send a message to the game server'''
    debug('maxev -> sending: "%s"' % line)
    sys.stdout.write(line + '\n')
    sys.stdout.flush()

def log(line):
    '''Write line to the log'''
    sys.stderr.write(line + '\n')
    sys.stderr.flush()

def debug(line):
    '''Write a line to the log only if in debug mode'''
    if DEBUG:
        log(line)

def parse_decision(decision):
    '''Parse the player's decision. Return None if invalid'''
    if decision in ('bluff', 'spoton'):
        return decision
    try:
        name, num, face = decision.split()
        num = int(num)
        face = int(face)
    except ValueError:
        return None
    return num, face

def play():
    '''The main input/output loop for playing the game'''
    while True:
        # Read message from the game server
        action, data = gets().split(': ', 1)

        # Start of a new game reset player data
        if action == 'players':
            numplayers = int(data)
            players = []
            playersdice = {}
        # Append player to player data
        elif action == 'player':
            players.append(data)
            playersdice[data] = 5  # Number of dice
        # Discover my own name
        elif action == 'you':
            myname = data
        # Update number of dice for player
        elif action == 'dicelost':
            playername, numdice = data.split()
            playersdice[playername] = int(numdice)
        # Player is out: remove from player data
        elif action == 'out':
            players.remove(data)
            playersdice.pop(data)
        # Start of a round (cannot bluff or spoton)
        elif action == 'round':
            last_bet = None
        # Someone has taken a turn (can bluff/spoton if this is a bet)
        elif action == 'turn':
            last_bet = parse_decision(data)
        # Remember what our dice are
        elif action == 'dice':
            debug('maxev -> dice -> ' + data)
            mydice = map(int, data.split())
        # Actually take my move
        elif action == 'go':
            mydecision = make_decision(
                last_bet, myname, mydice, players, playersdice)
            puts(mydecision)
        # Ignore unrecognised lines
        else:
            pass

# ::::::
#   Statistical functions
# ::::::

def cached(func):
    '''Decorator to cache return values'''
    cache = {}
    @functools.wraps(func)
    def wrapper(*args):
        try:
            return cache[args]
        except KeyError:
            cache[args] = val = func(*args)
            return val
    return wrapper

def cdf(k, N, p=Fraction('1/6')):
    '''cdf of the Binomial distribution.

    Returns Pr(X <= k) where X ~ B(N, p)

        >>> N = 6
        >>> for n in range(N + 1):
        ...     print(n, ':', cdf(n, 6))
        0 : 15625/46656
        1 : 34375/46656
        2 : 21875/23328
        3 : 23125/23328
        4 : 46625/46656
        5 : 46655/46656
        6 : 1
    '''
    return _cdf(k, N, p)[0]

def pmf(k, N, p=Fraction('1/6')):
    '''pmf of the Binomial distribution.

    Returns Pr(X = k) where X ~ B(N, p)

        >>> N = 6
        >>> for n in range(N + 1):
        ...     print(n, ':', pmf(n, N))
        0 : 15625/46656
        1 : 3125/7776
        2 : 3125/15552
        3 : 625/11664
        4 : 125/15552
        5 : 5/7776
        6 : 1/46656
        >>> # Probabilities sould add to 1
        >>> print(sum((pmf(n, N)) for n in range(N + 1)))
        1
    '''
    return _cdf(k, N, p)[1]

@cached
def _cdf(k, N, p):
    '''Helper for cdf. Returns the probability of and cdf for k, N, p'''
    if k == 0:
        pm = (1 - p) ** N
        return pm, pm
    else:
        cdf_n1, pm_n1 = _cdf(k - 1, N, p)
        # Pr(k) from Pr(k - 1)
        pm = pm_n1 * ((p / (1 - p)) * (N - k + 1)) / k
        return cdf_n1 + pm, pm

# ::::::
#   Artificial intelligence
# ::::::

def make_decision(last_bet, myname, mydice, players, playersdice):
    '''Actually make a decision about how to play

    Arguments::
        last_bet: the last bet made. None if this is the first turn
        myname: the name of this player as a string
        mydice: list of integers representing my dice
        players: list of names of all players in the order of play
        playersdice: dict mapping player names to their number of dice

    Returns::
        decision: string representing our decision

        >>> myname = 'oscar'
        >>> mydice = [3, 3, 4, 5]
        >>> players = ['dan', 'oscar', 'neej']
        >>> playersdice = {'dan': 2, 'oscar': 4, 'neej':5}
        >>> args = (myname, mydice, players, playersdice)
        >>> make_decision((1, 3), *args)
        '2 3'
        >>> make_decision((2, 3), *args)
        '3 3'
        >>> make_decision((3, 3), *args)
        'spoton'
        >>> make_decision((4, 3), *args)
        'bluff'
    '''
    # The actual AI for the player goes here

    # The idea:
    # 1. Make no attempt to draw inferences from the bets of other players.
    # 2. Use only the dice we can see to infer the probabilities of dice on
    # the table.
    # 3. Compute the expected value (EV) for bluff, spoton and different
    # raises. The EV is the difference between how many dice we have and the
    # average number of dice the other players have. Equivalently the EV is
    # the difference between our dice multiplied by the number of other
    # players and the total number of dice that other players have.
    # 4. Do not assume that the last player knew anything about the dice.
    # 5. Assume that the next player will play in the same way as us but
    # without having any information from their own dice.
    # 6. The probabilities for winning in each case are:
    #   bluff  : prob{other_dice <= last bet - our_dice}
    #   spoton : prob{other_dice == last_bet - our_dice}
    #   raiseb : prob{next_bluff(our_dice) and other_dice >= our_bet - our_dice}
    #   raises : prob{next_spoton(our_dice) and other_dice != our_bet - our_dice}
    #   raiser : prob{next_raise(our_dice)}
    # 7. The outcomes for each case are (n := number of other players):
    #   case   : win, lose
    #   bluff  : 1  , n
    #   spoton : n  , n
    #   raiseb : 1  , n
    #   raises : 1  , 1
    #   raiser : n/a   , n/a
    # 8. The EV for case j is
    #   EV(j) := prob(win | j) * winnings(j) - prob(lose | j) * losses(j)
    # 9. The move with maximal EV is selected. We consider calling bluff and
    # spoton and then consider the first 12 admissible raises. For each raise
    # we consider the EV contributions from the next player calling bluff or
    # spoton. For the best raise we consider what happens if we increase the
    # number of dice for the same face.
    # 10. Outcomes for bluff, spoton and the best raise are compared. The one
    # with the maximal EV is our decision.

    # Accumulate a map from each move to its EV
    possibilities = {}

    # Precompute the needed quantities
    if last_bet is None:
        # Use this as the base bet for considering raises
        num, face = 1, 0
    else:
        num, face = last_bet
    # Number of other players and the number of other dice
    numplayers = len(playersdice) - 1
    numdice = sum(playersdice.values()) - len(mydice)
    mydice = dict((n, mydice.count(n)) for n in range(6 + 1))

    # Args to all functions
    args = (num, face, mydice, numdice, numplayers)

    # Check this isn't the first round.
    if last_bet is not None:
        possibilities['bluff'] = bluff_ev(*args)
        possibilities['spoton'] = spoton_ev(*args)

    # Consider different possible raises
    for move, ev in raise_evs(*args):
        possibilities[move] = ev

    # Output all possibilties for debugging
    if DEBUG:
        debug('maxev -> last_move %d %d' % (num, face))
        debug('maxev -> my_dice    %s' % (mydice,))
        for move in sorted(possibilities, key=possibilities.__getitem__):
            debug('maxev -> poss_move %s %f' % (move, possibilities[move]))

    # Find the best move
    ev, move = max((ev, move) for move, ev in possibilities.items())
    if isinstance(move, tuple):
        move = '%d %d' % move

    # Check if a safe bet is available
    best = max_guaranteed(num, face, mydice, numdice, numplayers)
    if isinstance(move, tuple) and best != move:
        debug('overriding max: %s with %s' % (best, move))

    # Make the choice with the best EV
    return move

def bluff_pwin(num, face, mydice, numdice, numplayers):
    '''Probability of winning when calling bluff'''
    # No chance of winning if we have that many dice
    if num <= mydice[face]:
        return 0
    # Compute probability that unseen dice are less than the required amount
    else:
        return cdf(num - mydice[face] - 1, numdice)

def spoton_pwin(num, face, mydice, numdice, numplayers):
    '''Probability of winning when calling spoton'''
    if num < mydice[face]:
        return 0
    else:
        return pmf(num - mydice[face], numdice)

def bluff_ev(num, face, mydice, numdice, numplayers):
    '''Find the EV for calling bluff'''
    pwin = bluff_pwin(num, face, mydice, numdice, numplayers)
    return 1 * pwin - numplayers * (1 - pwin)

def spoton_ev(num, face, mydice, numdice, numplayers):
    '''Find the EV for calling spoton'''
    pwin = spoton_pwin(num, face, mydice, numdice, numplayers)
    return numplayers * pwin - numplayers * (1 - pwin)

def iter_raises(num, face, nraises):
    '''Iterate through the next nraises possible raises'''
    for n in range(nraises):
        face += 1
        if face > 6:
            num, face = num + 1, 1
        yield num, face

def raise_evs(num, face, mydice, numdice, numplayers):
    '''Find the EVs for different raises'''
    # Loop through the first 12 possible raises
    for num, face in iter_raises(num, face, 30):
        # Compute EV and yield bet and EV
        outcome = raise_ev(num, face, mydice, numdice, numplayers)
        yield (num, face), outcome

def max_guaranteed(num, face, mydice, numdice, numplayers):
    '''Find the maximum guaranteed safe bet'''
    maxcount = max(mydice.values())
    bestfaces = [n for n in range(1, 6 + 1) if mydice[n] == maxcount]
    return max(bestfaces), maxcount

def raise_ev_bluff(num, face, mydice, numdice, numplayers):
    '''Find the EV for this raise assuming next opponent bluffs'''
    # Assume that the opponent will bluff
    pwin = 1 - bluff_pwin(num, face, mydice, numdice, numplayers)
    return 1 * pwin - numplayers * (1 - pwin)

def raise_ev(num, face, mydice, numdice, numplayers):
    '''Find the EV for this raise'''
    # What would I do if I was the next player (and had no dice)
    theirdice = dict((n, 0) for n in range(1, 6 + 1))
    args = (theirdice, numdice + sum(mydice.values()), numplayers)

    possibilities = {}
    possibilities['bluff'] = bluff_ev(num, face, *args)
    possibilities['spoton'] = spoton_ev(num, face, *args)
    
    for their_num, their_face in iter_raises(num, face, 12):
        ev = raise_ev_bluff(their_num, their_face, *args)
        possibilities[(their_num, their_face)] = ev

    ev, their_move = max((ev, move) for move, ev in possibilities.items())

    # What's the probability that I win if they bluff after this raise?
    if their_move == 'bluff':
        pwin = 1 - bluff_pwin(num, face, mydice, numdice, numplayers)
        return 1 * pwin - numplayers * (1 - pwin)
    elif their_move == 'spoton':
        pwin = 1 - spoton_pwin(num, face, mydice, numdice, numplayers)
        return 1 * pwin - 1 * (1 - pwin)
    # 0 EV if they raise (this is kind of arbitrary)
    else:
        return 1 - bluff_pwin(num, face, mydice, numdice, numplayers)


if __name__ == "__main__":
    if '--test' in sys.argv:
        import doctest
        doctest.testmod()
    elif sys.argv[1:]:
        with open(sys.argv[1]) as INPUT:
            play()
    else:
        play()
