#!/usr/bin/env python
#
# liars.py
#
# Script to play Liar's dice with the AI players.


from __future__ import print_function


import os
import optparse
import json
import subprocess
import random
import time
import contextlib


GENERAL_CONFIG = 'config.json'
PLAYERS_CONFIG = 'players.json'
PLAYER_CONFIG_FILE = 'config.json'
PLAYERSDIR = 'players'


class InvalidMoveError(BaseException):
    '''Raised when a move from a player is invalid'''


class Config(object):
    '''
    This class just brings together a few functions used to load configuration
    data
    '''

    @classmethod
    def load_json(cls, fname, dir=None):
        if dir is not None:
            fname = os.path.join(dir, fname)
        with open(fname) as fin:
            return json.load(fin)

    @classmethod
    def load_players(cls, opts):
        '''Discover players in directory playersdir and load the player
        metadata.
        '''
        # Load the global config
        config = cls.load_json(opts.configfile)
        cmdlines = config['cmdline']

        # Read main metadata. This specifies what directories contain players and
        # whether or not the players are included.
        players_info = cls.load_json(PLAYERS_CONFIG, opts.playersdir)

        # Load the AI players
        players = []
        for name, meta in players_info.items():
            if meta['included']:
                playerdir = os.path.join(opts.playersdir, name)
                players.append(cls.load_player(name, playerdir, cmdlines))

        # Add the human players
        for name in opts.humans:
            players.append(HumanPlayer(name))

        return players

    @classmethod
    def load_player(cls, name, playerdir, cmdlines):
        '''Read configuration data required to run the player'''
        info = cls.load_json(PLAYER_CONFIG_FILE, playerdir)
        cmdargs = cmdlines[info['type']] + [info['script']]
        return Player(name, cmdargs, playerdir)


class Player(object):
    '''AI player running in a subprocess.'''

    def __init__(self, name, cmdargs, cwd):
        self.name = name
        self.cmdargs = cmdargs
        self.cwd = cwd
        self.process = None
        self.stderr = None

    def start(self, outputfile=None):
        '''Start the subprocess'''
        self.process = subprocess.Popen( self.cmdargs, cwd=self.cwd,
                stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                stderr=outputfile)

    def send(self, msgs):
        '''Use this to send messages to the player'''
        try:
            self.process.stdin.write('\n'.join(msgs) + '\n')
            self.process.stdin.flush()
        except:
            print('Problem writing to: ' + self.name, file=sys.stderr)
            raise

    def turn(self, timeout=20):
        '''Call this to find out the player's move

        This can lead to a deadlock if the player is waiting for more data'''
        return self.process.stdout.readline().strip()

    def kill(self):
        '''Always kill the player when done!'''
        if self.stderr is not None:
            self.stderr.close()
            self.stderr = None
        if self.process is not None:
            self.process.kill()
            self.process = None


class HumanPlayer(object):
    '''Human player. Allows a human to take part in the game via the
    terminal'''
    def __init__(self, name):
        self.name = name

    def send(self, msgs):
        for msg in msgs:
            print(msg)

    def turn(self):
        return raw_input()

    # Dummy method for compat with Player
    def kill(self): pass
    def start(self, outputfile=None): pass


class Game(object):
    '''This class manages a single game'''

    def __init__(self, players):
        self.players = list(players)

    def play(self, numgames, outputfile, debug=False):
        '''Play the game'''
        # Open the logs (and ensure they get closed later)
        with self._open_logs(outputfile, debug):

            # Start the player subproceses (and ensure they're stopped later)
            with self._start_players(outputfile, debug):

                # Actually play the game
                self._play(numgames)

    def _play(self, numgames):
        '''Actually play the games'''
        for gamenum in range(1, numgames + 1):
            self.initialise_game(gamenum)
            self.announce_game()

            while len(self.playing) > 1:
                self.round += 1
                dice = self.generate_dice()
                self.announce_round(dice)

                # last_bet is set to None after a bluff or spoton.
                last_bet = True
                while last_bet:
                    # Announce turn and get decision, then update game
                    decision = self.ask_player(last_bet)
                    self.announce_turn(decision)
                    last_bet = self.handle_decision(dice, decision, last_bet)

            # End of the game
            self.announce_winner(self.playing[0])

        # End of all games
        self.announce_gameover()

    def initialise_game(self, gamenum):
        # Randomise the order of players
        self.playing = list(self.players)
        random.shuffle(self.playing)

        # First player in the list is first to go
        self.gamenum = gamenum
        self.round = 0
        self.next_go = 0
        self.num_dice = dict((p.name, 5) for p in self.playing)

    def ask_player(self, last_bet):
        '''Ask the player what they want to do'''
        if last_bet in (None, True):
            moves = '"num face" (e.g. 3 4 for three fours)'
        else:
            moves = '"bluff", "spoton" or "num face" (e.g. 3 4 for three fours)'
        player = self.whose_turn
        self.send(player, ['go: {0}'.format(player.name), 'actions: ' + moves])
        return player.turn().lower()

    def handle_decision(self, dice, decision, last_bet):
        '''Process a decision from the current player and update state'''
        try:
            if decision in ('bluff', 'spoton'):
                if last_bet in (None, True):
                    raise InvalidMoveError('Cannot bluff/spot on! on first turn')
                if decision == 'bluff':
                    self.handle_bluff(dice, last_bet)
                else:
                    self.handle_spoton(dice, last_bet)
                return None
            else:
                bet = self.parse_bet(decision)
                self.handle_bet(bet, last_bet)
                return bet
        except InvalidMoveError as e:
            self.handle_invalid(e)

    def handle_bluff(self, dice, last_bet):
        '''Call the bluff need >= bet'''
        self.announce_dice(dice)
        num, face = last_bet
        count = sum(d.count(face) for d in dice.values())
        if count < num:
            self.increment_turn(step=-1)
        self.take_die()

    def handle_spoton(self, dice, last_bet):
        '''Call the spoton need == bet'''
        self.announce_dice(dice)
        num, face = last_bet
        count = sum(d.count(face) for d in dice.values())
        # Take a die from all other players
        if count == num:
            player = self.whose_turn
            self.increment_turn()
            while self.whose_turn is not player:
                self.take_die()
                self.increment_turn()
        # Take a die from the spoton caller
        else:
            self.take_die()

    def handle_bet(self, bet, last_bet):
        '''Validate that the bet was appropriate'''
        num, face = bet
        if last_bet not in (None, True):
            last_num, last_face = last_bet
            if num < last_num or (num == last_num and face <= last_face):
                raise InvalidMoveError('You need to raise the bet')
        self.increment_turn()

    def handle_invalid(self, error):
        '''Take a die from a player who gives an invalid move'''
        self.broadcast(['error: {0} "{1}"'.format(self.whose_turn.name, error)])
        self.take_die()

    def parse_bet(self, decision):
        '''Parse an input string describing someones bet'''
        try:
            num, face = [int(s) for s in decision.split()]
        except ValueError:
            raise InvalidMoveError('Unable to parse input move')
        return num, face

    def generate_dice(self):
        '''Return a new dict of dice entries'''
        dice = {}
        for p in self.playing:
            dice[p.name] = [random.randint(1, 6) for n in range(self.num_dice[p.name])]
        return dice

    def increment_turn(self, step=1):
        '''Increment the turn counter to find out who's go it is'''
        self.next_go = (self.next_go + step) % len(self.playing)

    def take_die(self):
        '''Remove a die from the current player'''
        player = self.whose_turn
        name = player.name
        self.num_dice[name] -= 1
        self.broadcast(['dicelost: {0} {1}'.format(name, self.num_dice[name])])
        if self.num_dice[name] == 0:
            self.player_out(player)

    def player_out(self, player):
        '''Remove a player from the game'''
        self.broadcast(['out: {0}'.format(player.name)])
        self.playing.remove(player)
        self.increment_turn(step=0)  # Fix the next_go counter

    @property
    def whose_turn(self):
        '''Return the player whose turn it is'''
        return self.playing[self.next_go]

    def broadcast(self, msgs):
        '''Send messages to all players'''
        msgs = list(msgs)
        if self.logfile is not None:
            for msg in msgs:
                self.logfile.write(msg + '\n')
        if self.debugfile is not None:
            for msg in msgs:
                self.debugfile.write('Broadcast: {0}\n'.format(msg))
        for p in self.players:
            p.send(msgs)

    def send(self, player, msgs):
        '''Send messages to player'''
        if self.debugfile is not None:
            for msg in msgs:
                self.debugfile.write('send -> {0}: {1}\n'.format(player.name, msg))
        player.send(msgs)

    def announce_game(self):
        '''Text to show at the start of the game'''
        lines = ['game: {0}'.format(self.gamenum),
            'players: {0}'.format(len(self.playing))]
        lines.extend('player: ' + p.name for p in self.playing)
        self.broadcast(lines)
        for p in self.playing:
            self.send(p, ['you: {0}'.format(p.name)])

    def announce_round(self, dice):
        '''Text to show at the start of each round'''
        self.broadcast(['round: {0}'.format(self.round)])
        for player in self.playing:
            d = dice[player.name]
            nums = ' '.join(map(str, d))
            self.send(player, ['dice: ' + nums])

    def announce_turn(self, decision):
        '''Text to show at the start of each turn'''
        decision = decision or '<no input>'
        self.broadcast(['turn: {0} {1}'.format(self.whose_turn.name, decision)])

    def announce_dice(self, dice):
        '''Show the dice at the end of a round'''
        lines = []
        for name, d in dice.items():
            nums = ' '.join(map(str, d))
            lines.append('showdice: {0} {1}'.format(name, nums))
        self.broadcast(lines)

    def announce_winner(self, player):
        '''Announce the winner at the end of a game'''
        self.broadcast(['winner: {0}'.format(player.name)])

    def announce_gameover(self):
        '''Announce the winner at the end of a game'''
        self.broadcast(['gameover: goodbye!'])

    @contextlib.contextmanager
    def _start_players(self, outputfile, debug):
        '''Context manager to start/stop the player subprocesses'''
        try:
            for player in self.players:
                # Open a logfile for each player?
                if outputfile:
                    filename = outputfile + '.' + player.name
                    player_outputfile = open(filename, 'a', buffering=1)
                # Player output direct to the terminal
                elif debug:
                    player_outputfile = None
                # Player output hidden
                else:
                    player_outputfile = subprocess.PIPE
                # Start the subprocess
                player.start(player_outputfile)

            # Execute the with block
            yield

        # Kill the players
        finally:
            for player in self.players:
                try:
                    player.kill()
                except:
                    msg = 'Unable to kill {0}.  Ignoring...'
                    print(msg.format(player.name), file=sys.stderr)

    @contextlib.contextmanager
    def _open_logs(self, outputfile, debug):
        '''Open log and debug output files'''
        self.logfile = self.debugfile = None

        try:
            if outputfile is not None:
                self.logfile = open(outputfile, 'a', buffering=1)
                if debug:
                    self.debugfile = open(outputfile + '.debug', 'a', buffering=1)
            # Output to stdout if no humans are playing and an outputfile was
            # not specified.
            elif not any(isinstance(p, HumanPlayer) for p in self.players):
                self.logfile = sys.stdout

            # Execute the with block
            yield

        finally:
            for f in self.logfile, self.debugfile:
                if f is not None:
                    try:
                        f.close()
                    except:
                        msg = 'Unable to close {0}.  Ignoring...'
                        print(msg.format(f.name), file=sys.stderr)


def main(*args):
    # Parse arguments and options e.g. --help
    parser = optparse.OptionParser(description="Play Liar's dice")
    parser.add_option('', '--human', dest='humans', action='append',
            default=[], help='Add a human player with the name NAME',
            metavar='NAME')
    parser.add_option('', '--players', dest='playersdir', default=PLAYERSDIR,
            help='Directory containing players', metavar='DIR')
    parser.add_option('', '--output', dest='outputfile', default=None,
            help='Directory for output files', metavar='FILE')
    parser.add_option('', '--config', dest='configfile',
            default=GENERAL_CONFIG, help='location of configuration file',
            metavar='FILE')
    parser.add_option('', '--debug', dest='debug', action="store_true",
            default=False, help='player output in terminal')
    parser.add_option('', '--numgames', dest='numgames', default=1, type=int,
            help='Number of games to play')
    opts, args = parser.parse_args(list(args))

    # This will be inherited by all child processes
    if opts.debug:
        os.environ['DEBUG'] = '1'

    # Read the configuration data for the players
    players = Config.load_players(opts)

    # Create the game
    game = Game(players)

    # Actually play the game
    game.play(opts.numgames, opts.outputfile, opts.debug)


if __name__ == "__main__":
    import sys
    main(*sys.argv[1:])
